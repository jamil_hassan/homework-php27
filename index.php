<!DOCTYPE html>
<html>
<head>
</head>
<body>

<h1>this is my first heading</h1>
<p>this is my first paragraph</p>
<?php 
//variable handling function testing: 
//this is a function declared
function jamil_hassan(){
	//declring two variables
$jamil="i am jamil hassan";
$jabid="i am jabid hassan";
//connect two variables using "."
$siblings=$jamil." and ".$jabid;
//echo this above variable
echo $siblings;
}
//this function has been called with echo 
echo jamil_hassan();
//another variable is declared
$jamil=true;
echo "</br>";
//         1. let's check if it is empty or not using empty function and if-else conditional statement
if (empty($jamil)){
	echo "this is empty";
}
else{
	echo "this is not empty";
}
echo "</br>";
//      2. let's check the data type of the last variable
echo gettype($jamil);
echo "</br>";
echo "<pre>";
// it is an index array in a variable!
$kichuekta=array('bangladesh','australia','india','america');
//get the output of the above array
print_r($kichuekta);
echo "</pre>";
echo "<pre>";
//this is an associative array(having key-value paires)
$onnokichu=array('a'=>'america','b'=>'bangladesh','c'=>'canada');
//get the output of the above array
print_r($onnokichu);
echo "</pre>";
//     3. let's check that if the given variable is an array or not!
if(is_array($onnokichu)){
	echo "<pre>";
	//get the output if it is an array
	print_r($onnokichu);
	}
else{
	//else not!
	echo "this is not an array!";
}
//this is an html code for testing purpose!
echo "<h1>this is an html code for testing purpose!</h1>";
//   4. let's make a float or integer value into string value 
$aro=293.90;
$change=strval($aro);
// and let check them!
if(is_string($change)){
	echo "Yes! this has been converted to string!";
}
else{
	echo "Yes! this has been converted to string!";
}
//   5. make an array to the string value using serialize 
echo "<pre>";
$my_serialize_data=serialize($onnokichu);
echo $my_serialize_data;
//echo the output string!
echo "</pre>";
//  6. again make it into array using unserialize
echo "<pre>";
$my_unserialize_data=unserialize($my_serialize_data);
//print the output array!
print_r ($my_unserialize_data);
echo "</pre>";
//  7. unset a value

$one=10;
$two="two";
echo $one;
echo "</br>";
echo $two;
//unset set a value to undefined!
unset($one);
echo $one;
//arrays into array: some complexity
$arr1=array(
'jamil'=>'hassan',
'jabid'=>'hassan',
'0'=>'sumaya',
'1'=>array(
'jabid'=>'one',
'juhi'=>'two',
'jamil'=>array('google','yahoo')


));
print_r($arr1);
echo ($arr1[1]['jamil'][0]);
echo "<pre>";
//   8. var_dump publishes all the informations of the given data!
$jamil=var_dump($arr1);
echo "</pre>";
echo "<pre>";

$arr2=array('one','two','three','four','five','six');
print_r($arr2);





echo "</pre>";
this is jamil hassan!






?>

</body>




</html>